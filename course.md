# GitLab for texts - Tutorial


## Which texts are considered here?

Here are just a few examples of texts in the field of education and science. Other areas in which texts or projects are worked with are of course just as conceivable ;-)

### School

* Lesson preparations [template]()
* Scripts with instructions and exercises
* Exam preparations

### Study

* Project work
* Student research projects
* Bachelor theses
* Master's theses

### Science

* Doctoral theses
* publications
* Project documentation


## What is GitLab?

GitLab is a web application for version management of - originally - software projects based on Git. [Wikipedia](https://de.wikipedia.org/wiki/GitLab)

What does this have to do with education? Not everyone programs! GitLab can also be used to manage, collaboratively edit, and publish multimedia texts for educational materials, student research papers, and more - **without cost, without installation, without infrastructure**!

GitLab offers

* Platform for managing (text) projects
* Hosting of websites
* version control
* User management for projects
* Project management

With a free account on GitLab, projects can be managed online in file directories. Comparable to a Dropbox, but with many more features, such as versioning, automation and user and rights management.

And this is what it looks like in practice ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/i8MxZU9w6bc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen"></iframe>

Examples

* [An example course](https://gitlab.com/TIBHannover/oer/course-metadata-test)
* [An example coursework](https://gitlab.com/axel-klinger/studienarbeit)


## A first project - My first course


Using the following steps to create a course in GitLab.

* [Registration/Application](https://gitlab.com/users/sign_in)
* Create a project (+ in the header)
  - Name: free name, precise and memorable
  - Public, to be visible for others
  - Create README, otherwise DEVELOPER HELL ;-)
* create document in project (+ in the middle)
* write in it, what the course is called and a paragraph as introduction

And here the whole thing again in the video ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/7vmyyFigXPQ" frameborder="0" allow="accelerometer" allowfullscreen="allowfullscreen"></iframe>


## A second project from template - My first OER course

### Advantages

* Output formats HTML, PDF and EPUB (and many more).
* TULLU tagging of Wikimedia Commons images.
* Machine readable metadata for discoverability
* License notice in the document

### Preparation
My preparation for a course:

* a browser and a login on GitLab.
* a topic for the course/unit
* a few snippets of text e.g. from [Wikipedia](https://de.wikipedia.org/wiki/Wikipedia:Hauptseite)
* a few images e.g. from [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page)


### Clone project - use a template

1. create new project -> + -> New Project -> Import project -> Repo by URL
  * [Template](https://gitlab.com/TIBHannover/oer/course-metadata-test.git) (copy link) and paste at "Git repository URL".
  * Project name - assign a meaningful name
  * Project slug - the same text, but replace spaces with hyphens and write out umlauts
  * make public
  * CREATE PROJECT
1. create and copy metadata with [generator](https://tibhannover.gitlab.io/oer/course-metadata-gitlab-form/metadata-generator.html)
1. paste metadata into metadata.yml
1. edit course.md with [Markdown](https://gitlab.com/axel-klinger/gitlab-for-documents/blob/master/gitlab-markdown.md)

After saving (Commit changes) it takes about 15min until the generated page is available under SETTINGS -> PAGES (as long as there is a 404). Further changes are usually available after <1min.

1. adjust addresses of output files in README with SETTINGS -> PAGES -> Your pages are served under
1. call page from README  

And here is the whole thing again in video ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/byRVOPM7qeg" allowfullscreen="allowfullscreen"></iframe>

... and here is the result ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/UfC0IWy87AI" allowfullscreen="allowfullscreen"></iframe>


## The interface of GitLab

Video to

* everything clicked once
* useful features also for OER


## Collaborating with GitLab

Video to

* issues
* merge requests
* editing rights


## Work offline more efficiently

Working offline has some advantages

* you always have a complete copy on your own computer
* many text editors have powerful tools for editing Markdown and other files

[Video with Atom more comfort for working with GitLab and Markdown]()


The disadvantage is

* if you don't synchronize cleanly, this can lead to conflicts - but meanwhile they can be solved quite well (e.g. in Atom)

[Video conflict due to parallel work on server].

Basically, any editor can be used for editing Markdown. The following shows a possible scenario using the Atom editor from https://atom.io.

### Atom

The editor offers good support for Markdown, extensible by some plaugins, and a good connection to GitHub/GitLab.

Installation

* Install Git from https://git-scm.com/downloads
* Install Atom from https://atom.io

A few key combinations in advance

| Function | Windows | Linux | Mac |
| -------------------------------- | ------------- | ----- | --- |
| Clone Project | ALT+G SHIFT+0 | |
| Show/Hide Project Structure | CTRL+K CTRL+B | | |
| Show/Hide Git View | CTRL+SHIFT+9 | |
| Full Screen Display | F11 | |
| Show/Hide Markdown Preview | CTRL+SHIFT+M | |

Keybindings can also be customized via File -> Settings -> Keybindings and calling the file "your keymap file".

## APPENDIX


## What are the formats of the texts?

* **Input :** Unformatted texts in [Markdown](gitlab-markdown.md). Later for advanced users also asciidoc or LaTeX.

* **Output :** HTML, PDF, EPUB ... and everything that supports [Pandoc](https://pandoc.org/)!


### Why version control for texts

As an example, let's take a study paper with about 50 pages, which in Word - including images - has a size of about 10 MB. Every time we save it under a new version, another 10+ MB are added, even if little has been changed.

The same file (the GitLab project) in this case has a size of about 3 MB in Markdown, because the biggest part in Word is Microsoft's own tags in the document and here only the pure content is saved. When we insert a change in GitLab, only the change with a few KB is saved as a version here. Not all included images and the whole document will be saved every time again.    

Other advantages include

* with Markdown: separation of content and layout supports reusability
* detailed history of changes


### The Markdown format

[Overview of elements](gitlab-markdown.md)


### Work offline more efficiently

* Using Atom as an example (other editors with good Markdown support and Git connectivity are also possible, such as Visual Studio Code or even various apps for GitHub/GitLab).

[Video on offline work with Atom]()

### Resolve conflicts!

> TODO !!!

### Basics of Git

* Installation

#### The Elements

* Repository
* Working Directory
* Commit
* Index
* Branch

#### The functions


The following listing is obtained by calling `git` without parameters

```
start a workspace (see also: git help tutorial)
   clone Clone a repository into a new directory
   init Create an empty git repository or reinitialize an existing repository

work on the current change (see also: git help everyday)
   add Add file contents to the index
   mv move or rename a file, directory or symlink
   reset Reset the current HEAD to the specified state
   rm Remove files from the working tree and from the index

examine history and state (see also: git help revisions)
   use bisect binary search to find the commit that introduced a bug
   grep Print lines that match a pattern
   log Display commit logs
   show Displays various types of objects
   status Displays the status of the working tree

grow, mark and tweak your common history
   list, create or delete branches
   checkout Change branches or restore working tree files
   commit Record changes in the repository
   diff View changes between commits, commit and working tree, etc.
   merge Merge two or more development histories
   rebase Transfer commits to another base tip
   tag Create, list, delete or verify a tag object signed with GPG

collaborate (see also: git help workflows)
   fetch Downloading objects and references from another repository
   pull Retrieve from and integrate with another repository or a local branch
   push Update remote repositories along with their associated objects
```
